package com.example.interfaceguiapiprduct;

import com.example.interfaceguiapiprduct.product.AddProduct;
import com.example.interfaceguiapiprduct.product.ListProduct;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.swing.*;
import java.awt.*;

@SpringBootApplication
public class InterfaceGuiApiPrductApplication {

	public static void main(String[] args) {

		JFrame mainFrame = new JFrame("Produit");
		mainFrame.setSize(new Dimension(800,800));
		mainFrame.setContentPane(new AddProduct().getJPanel());
		//mainFrame.setContentPane(new ListProduct().getListPanel());
		mainFrame.setVisible(true);
	}

}
