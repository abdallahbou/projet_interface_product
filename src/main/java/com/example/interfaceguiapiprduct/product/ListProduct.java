package com.example.interfaceguiapiprduct.product;

import lombok.Data;

import javax.swing.*;
import java.awt.*;

@Data
public class ListProduct {


    private JPanel listPanel;

    private GridBagConstraints constraints;

    private GridBagLayout bagLayout;


    private JList  itemList;

    private JList qtylist;

    private JList costList;

    private JComboBox productBox;

    private JComboBox qtyBox;


    private String[] name = new String[] {"Product Name ","Price Per Unit(£) ","Quantity "};
    private String[] name2 = new String[]{"Item ","Quantity ","Cost "};
    private String[] name3= new String[]{"", "Total Amount: "};


    public ListProduct(){
        create();
        createBox();
        createButton();
        createName2();
        createList();
        createName3();

    }

    public void create(){

        for (int i=0;i< name.length; i++){

            JLabel l = new JLabel(name[i]);
            constraints= new GridBagConstraints();
            constraints.gridx=i;

            listPanel.add(l,constraints);
        }
    }

    public void createName2(){
        for (int i=0;i< name2.length; i++){

            JLabel l = new JLabel(name2[i]);
            constraints= new GridBagConstraints();

            constraints.gridx=i;
            constraints.gridy=3;

            listPanel.add(l,constraints);
        }
    }
    public void createName3(){
        for (int i=0;i< name3.length; i++){

            JLabel l = new JLabel(name3[i]);
            constraints= new GridBagConstraints();

            constraints.gridx=i;
            constraints.gridy=6;

            listPanel.add(l,constraints);
        }
    }
    public void createButton(){

        JButton jButton = new JButton();
        jButton.setText("ADD PRODUCT");
        constraints = new GridBagConstraints();
//        jButton.setLayout(new GridLayout(4, 4));
//        jButton.setPreferredSize(new Dimension(300,20));





        constraints.fill= GridBagConstraints.BOTH;
        constraints.gridx=0;

        constraints.gridwidth=3;
        constraints.gridy=2;

        listPanel.add(jButton,constraints);


    }

    private void createList(){

        constraints = new GridBagConstraints();

        constraints.gridx=0;
        constraints.gridy = 4;
        constraints.fill = GridBagConstraints.HORIZONTAL;

        constraints.gridheight=3;

        itemList = new JList();
        constraints.gridx=0;
        listPanel.add(itemList,constraints);

        qtylist = new JList();
        constraints.gridx=1;
        listPanel.add(qtylist,constraints);

       costList = new JList();
        constraints.gridx=2;
        listPanel.add(costList,constraints);


    }

    private void createBox(){


        constraints = new GridBagConstraints();

        constraints.gridx=0;
        constraints.gridy=1;
        constraints.fill=GridBagConstraints.BOTH;

        productBox =new JComboBox();
        constraints.gridx=0;
        listPanel.add(productBox,constraints);

        qtyBox = new JComboBox();
        constraints.gridx=2;
        listPanel.add(qtyBox,constraints);

    }

}
