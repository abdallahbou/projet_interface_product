package com.example.interfaceguiapiprduct.product;

import lombok.Data;

import javax.swing.*;
import java.awt.*;

@Data
public class AddProduct {
    private JPanel jPanel;

    private GridBagLayout bagLayout;

    private JTextField idTextField;

    private JTextField nameTextField;

    private JTextArea description;

    private JTextField qtyTextField;

    private JTextField priceTextField;

    private JButton jButton;
    private String[] name = new String[]{"Product ID ", "Name ", "Description ", "Quantity in hand ", "Unit Price "};

    private String[] buttons = new String[]{"Add", "Update", " First", "Previous", "Next", "Last"};


    public AddProduct() {
        create();
        createButton();
        createFormLabel();
    }

    public void createButton() {


        int x = 0;
        int y = 5;
        for (String b : buttons) {

            jButton = new JButton(b);

            GridBagConstraints c = new GridBagConstraints();

            c.gridx = x;
            c.gridy = y;

            if (b.equals("Update")) {
                y++;
                x = 0;
            } else {
                x++;
            }

            jPanel.add(jButton, c);
        }

    }

    public void create() {

        for (int i = 0; i < name.length; i++) {
            for (int j = 0; j < 4; j++) {
                JLabel l = new JLabel(name[i]);
                GridBagConstraints c = new GridBagConstraints();


                c.gridy = i;

                if (i > 2) {
                    c.gridx = 2;

                } else {
                    c.gridx = 0;

                }
                jPanel.add(l, c);
            }
        }

    }

    public void createFormLabel() {

        GridBagConstraints constraints = new GridBagConstraints();

        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.fill = GridBagConstraints.BOTH;
        idTextField = new JTextField();
        constraints.gridy = 0;
        jPanel.add(idTextField, constraints);

        nameTextField = new JTextField("");
        constraints.gridy = 1;
        jPanel.add(nameTextField, constraints);

        description = new JTextArea();
        constraints.gridheight=3;
        constraints.gridy = 2;
        jPanel.add(description, constraints);

        qtyTextField= new JTextField();
        constraints.gridheight=1;
        constraints.gridy=3;
        constraints.gridx=3;
        jPanel.add(qtyTextField,constraints);

        priceTextField = new JTextField();
        constraints.gridheight=1;
        constraints.gridy=4;
        constraints.gridx=3;
        jPanel.add(priceTextField,constraints);

    }
}

